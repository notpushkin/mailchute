import os


import os
from httpx import AsyncClient


class ExampleHandler:
    def __init__(self) -> None:
        self.url = os.environ["HANDLER_URL"]
        self.client = AsyncClient()

    async def handle_RCPT(self, server, session, envelope, address, rcpt_options):
        # TODO: map URL for address
        envelope.rcpt_tos.append(address)
        return "250 OK"

    async def handle_DATA(self, server, session, envelope):
        print("Got an email for", envelope.rcpt_tos)
        resp = await self.client.post(
            self.url,
            headers={
                "Content-Type": "message/rfc822",
                "User-agent": "mailchute/0.1.0",
            },
            content=envelope.content,
        )
        if resp.ok:
            return "250 Message accepted for delivery"
        else:
            return "450 idk it didn't work"
