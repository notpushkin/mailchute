import asyncio

from aiosmtpd.controller import Controller

from mailchute.app import ExampleHandler

async def halt():
    while True:
        await asyncio.sleep(3600)


controller = Controller(ExampleHandler(), hostname="0.0.0.0")
controller.start()
print(f"Listening on {controller.hostname}:{controller.port}...")
asyncio.run(halt())
